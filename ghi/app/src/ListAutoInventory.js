import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

function ListAutoInventory() {
    const [autos, setAutos] = useState([])
    
    const fetchData =async() => {
        const response = await fetch('http://localhost:8100/api/automobiles')
        if (response.ok) {
            const data = await response.json()
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        fetchData();
      }, []);
    return(
        <>
            <h1>List of Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Vin</th>
                        <th scope="col">Color</th>
                        <th scope="col">Year</th>
                        <th scope="col">Manufacturer</th>
                        <th scope="col">Vehicle Model</th>
                        <th scope="col">Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        if (!auto.sold) {
                            return (
                                <tr key={auto.vin}>
                                    <td>{ auto.vin }</td>
                                    <td>{ auto.color }</td>
                                    <td>{ auto.year }</td>
                                    <td>{ auto.model.manufacturer.name }</td>
                                    <td>{ auto.model.name }</td>
                                    <td> In Stock</td>
                                </tr>
                            );
                            }})}
                </tbody>
            </table>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Link to="/automobiles/new" className="btn btn-primary btn-lg px-4 gap-3">Add a new auto</Link>
            </div>
        </>
    )
}

export default ListAutoInventory
