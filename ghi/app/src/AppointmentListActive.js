import React, { useEffect, useState} from "react";
import { Link } from 'react-router-dom';

const complete = async(event) =>{
    event.preventDefault();
  const id = event.target.value;
  const urlComp =`http://localhost:8080/api/appointment/${id}/`
  console.log(id, urlComp)

  const fetchConfig = {
    method: "PUT",
    body: JSON.stringify({ "status" : "completed"}),
    headers: {
      "Content-Type": "application/json",
        },
    };
    const response = await fetch(urlComp, fetchConfig)
    if (response.ok) {
        console.log("appointment completed");
        
      } else {
        console.log("failed to complete")
      }
}


const cancelled = async(event) =>{
    event.preventDefault();
  const id = event.target.value;
  const urlComp =`http://localhost:8080/api/appointment/${id}/`
  console.log(id, urlComp)

  const fetchConfig = {
    method: "PUT",
    body: JSON.stringify({ "status" : "cancelled"}),
    headers: {
      "Content-Type": "application/json",
        },
    };
    const response = await fetch(urlComp, fetchConfig)
    if (response.ok) {
        console.log("appointment cancelled");
       
        
      } else {
        console.log("failed to cancelled")
      }
}
// testing outside of function
// async function GetData(){
//   const [AppointmentlList, setList] = useState([]);
//   const [AppointmentlListVIP, setListVIP] = useState([]);
        
//   try{
      

//       if (response.ok){
//           const data =  await response.json();
//           const datavip = data.appointment;
//           const dataVO =  await responseVO.json();
//           const vipAutos = []
          
//          for (let vipAuto of dataVO.automobileVO){
//           vipAutos.push(vipAuto.vin);
//          }
//          setListVIP(vipAutos)
//           console.log("dataVO", dataVO);
//           for (let apt of datavip){
//               if (AppointmentlListVIP.includes(apt.vin)){
//                   apt.vip = "yes";
//               }else{
//                   apt.vip = "no";
//               }

//           }

//           setList(datavip);
          
//       }
//   }catch (e) {
//       console.error(e);
//   }
// }

// ____________________________________________________
const ActiveAppointmentlList = (props) =>{
    const [AppointmentlList, setList] = useState([]);
    const [AppointmentlListVIP, setListVIP] = useState([]);
    async function GetData(){
        
        try{
            
        const response = await fetch("http://localhost:8080/api/appointments_act/");
        const responseVO = await fetch("http://localhost:8080/api/AutomobileVO/");
            if (response.ok){
                const data =  await response.json();
                const datavip = data.appointment;
                const dataVO =  await responseVO.json();
                const vipAutos = []
                
               for (let vipAuto of dataVO.automobileVO){
                vipAutos.push(vipAuto.vin);
               }
               setListVIP(vipAutos)
                console.log("dataVO", dataVO);
                for (let apt of datavip){
                    if (AppointmentlListVIP.includes(apt.vin)){
                        apt.vip = "yes";
                    }else{
                        apt.vip = "no";
                    }
 
                }

                setList(datavip);
                
            }
        }catch (e) {
            console.error(e);
        }
    }
useEffect(() => {
    GetData();
  }, []);
  return(
    <>
  <table className="table table-striped">
    <thead>
        <tr> 
            <td>date</td>
            <td>reason</td>
            <td>status</td>
            <td>vin</td>
            <td>VIP</td>
            <td>customer</td>
            <td>technician</td>
            <td></td>
            <td></td>

        </tr>
    </thead>
    <tbody>
        {AppointmentlList.map(appointment => {
            return(
                <tr key={appointment.id}>                
                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                <td>{appointment.vin}</td>
                <td>{appointment.vip}</td>
                <td>{appointment.customer}</td>                      
                <td>{appointment.technician.first_name}</td>
                <td><button type="button"  value={appointment.id} onClick={(event) => cancelled(event, appointment.id)} className="btn btn-danger">cancell</button></td>
                <td><button type="button" value={appointment.id} onClick={(event) => complete(event, appointment.id)} className="btn btn-success">completed</button></td>
                </tr>
            )
        })}
    </tbody>
</table>
<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
    <Link to="/appointments/new" className="btn btn-primary btn-lg px-4 gap-3">Add appointment</Link>
</div>
    </>
  );
  }

export default ActiveAppointmentlList;