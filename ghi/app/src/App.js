import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ActiveAppointmentlList from './AppointmentListActive';
import AppointmentlVinList from './AppointVinHistory';
import MainPage from './MainPage';
import MakeList from './manufactureList';
import MakeForm from './newManufacturer';
import ModelList from './modelList';
import ModelForm from './newModel';
import NewAppointment from './newAppointment';
import TechList from './TechList';
import TechForm from './newTech';

import Nav from './Nav';
import ListAutoInventory from './ListAutoInventory';
import NewAutoForm from './NewAutoForm';
import ListCustomers from './listCustomers';
import NewCustomer from './newCustomerForm';
import ListSalespeople from './listSalespeople';
import NewSalesperson from './newSalespersonForm';
import SalespersonHistory from './SalespersonHistory';
import ListSales from './listSales';
import NewSale from './newSale';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="Make">
            <Route index element={<MakeList />} />
            <Route path="new" element={<MakeForm />} />
          </Route>

          <Route path="model">
            <Route index element={<ModelList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>

          <Route path="techs">
          <Route index element={<TechList />} />
          <Route path="new" element={<TechForm />} />         
          </Route>

          <Route path="appointments">
          <Route path="active" element={<ActiveAppointmentlList />} />
          <Route path="vin" element={<AppointmentlVinList />} />
          <Route path="new" element={<NewAppointment />} />         

                  
          </Route>
          
          {/* below line for other links */}
          <Route path="automobiles">
            <Route index element={<ListAutoInventory />} />
            <Route path="new" element={<NewAutoForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<ListCustomers />} />
            <Route path="new" element={<NewCustomer />}/>
          </Route>
          <Route path="salespeople">
            <Route index element={<ListSalespeople />} />
            <Route path="new" element={<NewSalesperson />}/>
            {/* ask Dan for his thoughts on where history should go */}
            <Route path="history" element={<SalespersonHistory />}/>
          </Route>
          <Route path="customers">
            <Route index element={<ListCustomers />} />
            <Route path="new" element={<NewCustomer />}/>
          </Route>
          <Route path="sales">
            <Route index element={<ListSales />} />
            <Route path="new" element={<NewSale />}/>
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
