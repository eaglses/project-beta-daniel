import React, { useEffect, useState} from "react";
import { Link } from 'react-router-dom';
const ModelList = () =>{
    const [ModelList, setList] = useState([]);
    async function GetModel(){
        
        try{
            
        const response = await fetch("http://localhost:8100/api/models/");
            if (response.ok){
                const data =  await response.json();
                setList(data.models);
            }
        }catch (e) {
            console.error(e);
        }
    }
useEffect(() => {
    GetModel();
  }, []);
  return(
    <>
  <table className="table table-striped">
    <thead>
        <tr> 
            <th scope="col">Name</th>
            <th scope="col">Picture</th>
            <th scope="col">Manufacturer Name</th>
            <th scope="col">ID</th>
        </tr>
    </thead>
    <tbody>
        {ModelList.map(model => {
            return(
                <tr key={model.href}>
                <td>{model.name}</td>
                <td>
                    <img src={model.picture_url} class="rounded float-start" />
                </td>
                <td>{model.manufacturer.name}</td>
                <td>{model.id}</td>
                </tr>
            )
        })}
    </tbody>
</table>
<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
    <Link to="/model/new" className="btn btn-primary btn-lg px-4 gap-3">Add a new model</Link>
</div>
    </>
  );
  }

export default ModelList;