import { useState, useEffect } from "react";


function ListSales() {
    const [sales, setSales] = useState([])

    const fetchData =async() => {
    const response = await fetch('http://localhost:8090/api/sales/')
        if (Response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <>
        <h1>List of Sales</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th scope="col">VIN</th>
                    <th scope="col">Salesperson</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{ sales.vin }</td>
                                <td>{ sales.customer }</td>
                                <td>{ sales.salesperson }</td>
                                <td>{ sales.price }</td>
                            </tr>
                        );
                        })}
            </tbody>
        </table>

        </>
    )
    
    }



export default ListSales
