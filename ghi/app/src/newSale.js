import { useState, useEffect } from "react";


function NewSale() {
    const [autos , setAutos] =useState([])
    const [salespeople , setSalespeople] =useState([])
    const [customers , setCustomers] =useState([])

    const [ formData, setFormData] = useState({
        employee_id: '',
        customer: '',
        vin: '',
        price: '',
    })
    const fetchData = async () => {
        const salespeople_url = "http://localhost:8090//api/salespeople/";
        const salespeopleResponse = await fetch(salespeople_url);
        if (salespeopleResponse.ok) {
            const salespeopleData= await salespeopleResponse.json();
            console.log(salespeopleData)
            setSalespeople(salespeopleData.salespeople);
        }
    }
    const fetchData2 = async () => {
        const autos_url = "http://localhost:8100/api/automobiles/";
        const autosResponse = await fetch(autos_url);
        if (autosResponse.ok) {
            const autosData= await autosResponse.json();
            setAutos(autosData.autos);
        }
    }
    const fetchData3 = async () => {
        const customers_url = "http://localhost:8090//api/customers/";
        const customersResponse = await fetch(customers_url);
        if (customersResponse.ok) {
            const customersData= await customersResponse.json();
            setCustomers(customersData.customers);
        }
    }

    useEffect(() => {
        fetchData();
        fetchData2();
        fetchData3();
    }, [] )
    const handleSubmit = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/api/sales/'        
        const fetchConfig = {
            method: "post",
            body :JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                employee_id: '',
                customer: '',
                vin: '',
                price: '',        
            })
        }

    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <>
            <div>
                <h1>New Sale</h1>
                <form id="create-sale-form" onSubmit={handleSubmit}>
                    <div>
                        <label htmlFor="employee_id">Employee_id</label>
                        <select onChange={handleFormChange} value={formData.employee_id} className="form-select" name="employee_id" id="employee_id"  required>
                            <option value="">Choose a model</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.employee_id} value={salesperson.employee_id}>  {salesperson.first_name + " " + salesperson.last_name}  </option>
                                )
                            }
                            )}
                        </select>
                    </div>
                    <div>
                        <label htmlFor="vin">VIN</label>
                        <select onChange={handleFormChange} value={formData.vin} className="form-select" name="vin" id="vin"  required>
                            <option value="">Choose a VIN</option>
                            {autos.map(auto => {
                                return (
                                    <option key={auto.vin} value={auto.vin}>  {auto.vin} </option>
                                )
                            }
                            )}
                        </select>
                    </div>
                    <div>
                        <label htmlFor="customer">Customer</label>
                        <select onChange={handleFormChange} value={formData.customer} className="form-select" name="customer" id="customer"  required>
                            <option value="">Choose a model</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.phone_number} value={customer.phone_number}>  {customer.first_name} {customer.last_name} </option>
                                )
                            }
                            )}
                        </select>
                    </div>
                    <div>
                        <label htmlFor="price">Price</label>
                        <input onChange={handleFormChange} value={formData.price} type="" name="price" id="price" placeholder="Price" required/>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        <p>This is a create sale page.</p>
        </>
    )
}


export default NewSale
