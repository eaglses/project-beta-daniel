import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success" >
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Car sales and services </NavLink>
        

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          
            {/* inventory dropdown*/}
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle"   role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory Dept Links
              </a>
              <ul className="dropdown-menu">
                <li className='dropdown-item'>
              <NavLink className="dropdown-item" to="/automobiles">Automobiles List</NavLink>
                </li>

                <li className='dropdown-item'>
              <NavLink className="dropdown-item" to="Make">Manufacturer</NavLink>
                </li>

                <li className='dropdown-item'>
              <NavLink className="dropdown-item" to="Make/new">AddManufacturer </NavLink>
                </li> 

                <li className='dropdown-item'>
              <NavLink className="dropdown-item" to="model">Models </NavLink>
                </li>

                <li className='dropdown-item'>
              <NavLink className="dropdown-item" to="model/new">Add Model </NavLink>
                </li>   

                <li className='dropdown-item'>
              <NavLink className="dropdown-item" to="/automobiles/new">Create an Automobile</NavLink>
                </li>        
        
              </ul>
            </li>
            {/* end inventory dropdown */}

            {/* sales dropdown*/}
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle"   role="button" data-bs-toggle="dropdown" aria-expanded="false">
                sales Dept Link
              </a>
              <ul className="dropdown-menu">

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="/sales">List All Sales</NavLink>
                </li> 

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="/customers">Customers</NavLink>
                </li>

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="/customers/new">New Customer</NavLink>
                </li>   

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="/salespeople/history">Salesperson history</NavLink>
                </li>

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="/sales/new">Record New Sale</NavLink>
                </li>         

              </ul>
            </li>
            {/* end sales dropdown */}

            {/* dropdown service*/}
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle"   role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service Dept Links
              </a>
              <ul className="dropdown-menu">


                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="appointments/active">Active appointments </NavLink>
                </li>
              
                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="appointments/new">New appointments </NavLink>
                </li>
  
                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="appointments/vin">service history </NavLink>
                </li> 
            
              </ul>
            </li>
            {/* end service dropdown */}

            {/* employee dropdown*/}
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle"   role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Employee links
              </a>
              <ul className="dropdown-menu">

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="/salespeople/new">New Salesperson</NavLink>
                </li>

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="techs/new">Add Technicians </NavLink>
                </li>

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="/salespeople">Sales people</NavLink>
                </li>

                <li className='dropdown-item'>
                  <NavLink className="dropdown-item" to="techs">technicians </NavLink>
                </li>

              </ul>
            </li>
            {/* end employee dropdown */}
 
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
