import React, { useEffect, useState } from 'react';

function NewAuto() {
    const [ modelList, setModel] = useState([])
    const [ formData, setFormData ] = useState({
        "color": "",
        "year": "",
        "vin": "",
        "model_id": ""
    })
    const fetchData = async () => {
        const url = `http://localhost:8100/api/models/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModel(data.models);
            console.log(data.models)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

     
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;    
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
const handleSubmit = async (event) => {
    event.preventDefault();  

    const url = `http://localhost:8100/api/automobiles/`;
    
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(formData),
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const response = await fetch(url, fetchConfig);
    console.log(formData)
    console.log("post>>>>>>>>>>", response)

    if (response.ok) {
        setFormData({
            "color": "",
            "year": "",
            "vin": "",
            "model_id": ""
         })
    }
}



return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add New Car to Inventory</h1>
          <form onSubmit={handleSubmit} id="create-model-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"  />
              <label htmlFor="vin">Please enter Vin</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.year} placeholder="year" required type="number" name="year" id="year" className="form-control"  />
              <label htmlFor="year">Please Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control"  />
              <label htmlFor="color">Please color</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                <option value="">what Model is it?</option>
                {modelList.map(model => {
                  return (
                    <option key={model.id} value={model.id}>{model.name}</option>
                  )
                })}
              </select>
            </div>        
          
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
)

}

export default NewAuto;
