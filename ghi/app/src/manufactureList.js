import React, { useEffect, useState} from "react";
import { Link } from 'react-router-dom';

const MakeList = (props) =>{
    const [MakeList, setList] = useState([]);
    async function GetMake(){   
        try{            
        const response = await fetch("http://localhost:8100/api/manufacturers/");
            if (response.ok){                
                const data =  await response.json();
                setList(data.manufacturers);
            }
        }catch (e) {
            console.error(e);
        }
    }

useEffect(() => {
    GetMake();
  }, []);

  return(
    <>
  <table class="table table-striped">
    <thead>
        <tr> 
            <td>name</td>
            <td>ID</td>
        </tr>
    </thead>
    <tbody>
        {MakeList.map(make => {
            return(
                <tr key={make.href}>
                <td>{make.name}</td>
                <td>{make.id}</td>
                </tr>
            )
        })}
    </tbody>
</table>
<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
    <Link to="/Make/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Make</Link>
</div>
    </>
  );
  }


export default MakeList;