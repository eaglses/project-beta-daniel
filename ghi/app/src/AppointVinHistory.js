import React, { useEffect, useState} from "react";





function AppointmentlVinList(){
    const [ vin, setvin ] = useState({vin:''})
    const [AppointmentlList, setList] = useState([]);
    const [AppointmentlListVIP, setListVIP] = useState([]);

    
    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log("we got this far")

        const response = await fetch(`http://localhost:8080/api/appointments/${vin.vin}/`);
        const responseVO = await fetch("http://localhost:8080/api/AutomobileVO/");
            if (response.ok){
                const data =  await response.json();
                const datavip = data.appointment;
                const dataVO =  await responseVO.json();
                const vipAutos = []
                
               for (let vipAuto of dataVO.automobileVO){
                vipAutos.push(vipAuto.vin);
               }
               setListVIP(vipAutos)
                console.log("dataVO", dataVO);
                for (let apt of datavip){
                    if (AppointmentlListVIP.includes(apt.vin)){
                        apt.vip = "yes";
                    }else{
                        apt.vip = "no";
                    }
                 }
                setList(datavip);     
            }
        };
       


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
    
        setvin({
            ...vin,
            [inputName]: value
        });
        

    }

  
  return(
    <>
    <form onSubmit={handleSubmit} id="search-vin">
    <div className="form-floating mb-3">
        <input onChange={handleFormChange} value={vin.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"  />
    </div>
    <button className="btn btn-primary">search your vin</button>
    </form>

    <table className="table table-striped">
    <thead>
        <tr> 
            <td>date</td>
            <td>reason</td>
            <td>status</td>
            <td>vin</td>
            <td>VIP</td>
            <td>customer</td>
            <td>technician</td>
        </tr>
    </thead>
    <tbody>
        {AppointmentlList.map(appointment => {
            return(
                <tr vin={appointment.id}>                
                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                <td>{appointment.vin}</td>
                <td>{appointment.vip}</td>
                <td>{appointment.customer}</td>                      
                <td>{appointment.technician.first_name}</td>
                </tr>
            )
        })}
    </tbody>
</table>
  

    </>
  );
  }

export default AppointmentlVinList;