import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here. Ignore vs-code error hinting
from service_rest.models import AutomobileVO


def get_autos():
   url = "http://project-beta-daniel-inventory-api-1:8000/api/automobiles/"
   response = requests.get(url)
   content = json.loads(response.content)
#    print(">>>>>>>>>>", content["autos"])
   for auto in content["autos"]:
       AutomobileVO.objects.update_or_create(
           vin=auto["vin"],
           defaults={
               "sold": auto["sold"],
                }
        )
       

def poll(repeat=True):
    while True:
        print('Service poller polling for data')
        try:
            # Write your polling logic, here
            # Do not copy entire file
            get_autos()
        
        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(30)


if __name__ == "__main__":
    poll()
